$(document).ready(function(){
	var ipconfig = new ipConfig();
	// alert(ipconfig.ip);

	$("#login").on("submit", function(e){
		e.preventDefault();
		var credentials = $(this).serialize();
		$('#loadermodal').modal("show");
		$('#loadermessage').html("Logging in, please wait...");
		$.ajax({
			type: "get",
			url: "http://"+ipconfig.ip+"/phpFile/actions/login.php",
			data: credentials,
			success: function(response){
				$('#loadermodal').modal("hide");
				var obj = $.parseJSON(response);
				if(obj.group == 5){
					localStorage.setItem("acct_type", obj.group);
					localStorage.setItem("userlogin", obj.userlogin);
					localStorage.setItem("fullname", obj.fullname);

					window.location.replace("scanrack.html?acct_type="+obj.group+"&userlogin="+obj.userlogin+"&fullname="+obj.fullname+"&usertype=Counter");
				}else if(obj.group == 4){
					localStorage.setItem("acct_type", obj.group);
					localStorage.setItem("userlogin", obj.userlogin);
					localStorage.setItem("fullname", obj.fullname);

					window.location.replace("dashboard.html?acct_type="+obj.group+"&userlogin="+obj.userlogin+"&fullname="+obj.fullname+"&usertype=Verifier");
				}else{
					alert("Username or Password is incorrect!");
				}
			},
        error: function(err){
          alert(err.responseText);
          window.location.reload();
        }
		});
	});
});